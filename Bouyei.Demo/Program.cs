﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Bouyei.Geo;
using System.IO;
using Bouyei.DbFactory;
 
namespace Bouyei.Demo
{
    using Bouyei.Geo.Geometries;
    using Bouyei.Geo.GeoParsers;

    class Program
    {
        static void Main(string[] args)
        {
            //TestVctFormat();

            //TestDbf();

            //TestGeoJson();

            //var item=JsonPaser.Json.Parse("{\"type\": \"Feature\",\"geometry\": {\"type\": \"Point\",\"coordinates\": [125.6, 10.1]},\"properties\": {\"name\": \"Dinagat Islands\"}}");

            //TestFromEsriMdb();

            //TestWkbFromPg();

            TestPrjParser();

             //TestArea();
            //TestShpfile();
        }

        static void TestWkbFromPg()
        {
            string connstr = "Server=127.0.0.1;Port=5432;User Id=postgres;Password=bouyei;Database=geodb;";
            using (IAdoProvider provider = AdoProvider.CreateProvider(connstr, FactoryType.PostgreSQL))
            {
                var param = new Parameter("select bsm,st_asbinary(geom) as wkb from public.\"DLTB522631\" WHERE objectid=1");
                var tb = provider.Query(param);

                var rt = provider.Query<Item>(param);
                var items = rt.Result;
                List<Geometry> geos = new List<Geometry>();
                foreach (var item in items)
                {
                    WkbParser wkbParser = new WkbParser(item.wkb);
                    var geo = wkbParser.FromReader();
                    geos.Add(geo);
                }
            }
        }

        static void TestVctFormat()
        {
            VctParser vct = new VctParser(@"E:\2001H2019522623.vct", Encoding.GetEncoding("GB2312"));
            var vctInfo = vct.FromReader();
        }

        static void TestFromEsriMdb()
        {
            string connstr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\\b.mdb;";
            using (IAdoProvider provider = AdoProvider.CreateProvider(connstr, FactoryType.OleDb))
            {
                var rt = provider.Query<Item>(new Parameter("select SHAPE as wkb from b where objectid=57"));
                var items = rt.Result;
                List<Geometry> geos = new List<Geometry>();
                foreach (var item in items)
                {
                    EsriMdbParser wkbParser = new EsriMdbParser(item.wkb);
                    var geo = wkbParser.FromReader();

                    //生成wkt
                     string wkt= geo.ToWkt();

                    geos.Add(geo);
                }
            }
        }

        class jsonattr
        {
            public int OBJECTID { get; set; }
        }

        static void TestArea()
        {
            //string str = File.ReadAllText("E:\\holepart.json");

            //GeoJsonParser geojson = new GeoJsonParser(str);
            //var item= geojson.FromReaderToFeatureCollection<jsonattr,JsonPolygon>();
            //foreach (var f in item.features)
            //{

            //}

            GeometryBase gbase = new GeometryBase();
           var b= gbase.XYtoLB(new Coordinate() { 
             X= 35000000+(699536.3496025258-1),
             Y= 3143915.3497209605+1
            });


            var b2 = gbase.XYtoLB(new Coordinate() {
                X = 35000000 + (699536.3496025258 - 1)+(511*2),
                Y = 3143915.3497209605 +1-(511*2)
            });


           var XY= gbase.LBtoXY(new Coordinate() { 
             X= 107.035646932681,
             Y= 28.3951210285414
            });

            string wktstr =string.Empty;//  File.ReadAllText("E:\\wkthole.txt");
            wktstr = "POLYGON ((105.66278148717595 26.268896196702144,105.65603151224457 26.270494874738745,105.6553345349609 26.27166525334883,105.65733907973481 26.273402525905908,105.6625862344672 26.271431379254807,105.66278148717595 26.268896196702144))";
            WktParser wkt = new WktParser(wktstr);
            var inter= wkt.FromReader();

            Geometry geometry = new Geometry(GeoType.POLYGON, inter);
            var seq= geometry.GetSequence(0, 0);
            var seq2 = geometry.GetSequence(0, 1);

            var dir= seq.GetOrientation();
            var dir12 = seq2.GetOrientation();

            //string wktstr = "POLYGON((0 0,0 2,2 2,2 0,0 0))";
            //using (FileStream fs = new FileStream("D:\\wkt.txt", FileMode.Open))
            //{
            //    using (StreamReader reader = new StreamReader(fs))
            //    {
            //        wktstr = reader.ReadToEnd();
            //    }
            //}

            GeometryPlane plane = new GeometryPlane();
            var area = plane.Area(wktstr);

            GeometryEllipse geo = new GeometryEllipse();
            var ellipse = geo.Area(wktstr);

            var dist = geo.Distance(new Coordinate()
            {
                X = 36379440.1493,
                Y = 2936717.206599999
            },
             new Coordinate()
             {
                 X = 36379425.4384,
                 Y = 2936710.4860999994
             });

            var geopoint = new Coordinate()
            {
                X = 36367729.9624,
                Y = 2941185.8308
            };

            var bl = plane.XYtoLB(geopoint);
            var bxy = plane.LBtoXY(bl);
        }

        static void TestPrjParser()
        {
            string file =@"F:\\prj.xml";
           var r= new PrjParser(file).FromReader();
        }

        static void TestGeoJson()
        {
            string file = "C:\\3DCity.json";// AppContext.BaseDirectory + "testfiles\\feature.geojson";//"C:\\3DCity.json";
            string content = File.ReadAllText(file, Encoding.UTF8);
            GeoJsonParser json = new GeoJsonParser(content);

            var geo = json.FromReaderToFeatureCollection<attr, JsonMultiPolygon>();

            List<double[]> coords = new List<double[]>();
            coords.Add(new double[] { 1, 2 });
            coords.Add(new double[] { 2, 3 });

            var collection = new FeatureCollection<attr, JsonLineString>()
            {
                name = "八渡镇",
                features = new Feature<attr, JsonLineString>[] {
                 new Feature<attr, JsonLineString>(){
                 properties=new attr(){  name="乃言村",code="522327"},
                 geometry=new JsonLineString(){
                 coordinates=coords
              }
             }
            }
            };
            var str = json.ToWriter<attr, JsonLineString>(collection);
        }

        static void TestDbf()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("name", typeof(string));
            dt.Columns.Add("age", typeof(int));
            dt.Columns.Add("score", typeof(float));
            dt.Columns.Add("range", typeof(double));
            dt.Columns.Add("date", typeof(DateTime));

            dt.Rows.Add(new object[] { "abb", 123, 33.4f,12.4444 ,DateTime.Now });
            dt.Rows.Add(new object[] { "ccc", 13, 33.44f, 12.5555,DateTime.Now });
            dt.Rows.Add(new object[] { "第df方", 12, 33.45f, 12.6666666,DateTime.Now });

            List<attr> list = new List<attr>();
            list.Add(new attr() {
                code = "和",
                name = "sdf",
            });
            list.Add(new attr() { 
            code="dsfasd1321",
             name="sdf对方"
            });

            DbfParser dbf = new DbfParser("test1.dbf");
            //dbf.ToWrite<attr>(list, null);

            dbf.ToWriter(dt, Encoding.GetEncoding("GB2312"));
        }

        static void TestShpfile()
        {
            string file = "F:\\县级行政区4.shp";
            ShpParser parser = new ShpParser(file);
            var geos= parser.FromReader();

            File.WriteAllText("f:\\test.wkt", geos.Features[0].geometry.ToWkt());
        }
    }

    public class attr
    {
        public string name { get; set; }

        public string code { get; set; }
    }

    public class Item
    {
        public string bsm { get; set; }

        public byte[] wkb { get; set; }

    }

}
