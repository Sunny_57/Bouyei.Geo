﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.GeoCore.GeoParsers.Dbf
{
    public class DbfFile : IDisposable
    {
        private readonly Encoding _encoding=Encoding.ASCII;
        private readonly BinaryWriter _writer=null;
        private DbfHeader _header;
        private bool _headerWritten;
        private string dbfFileName = string.Empty;

        public DbfFile(string dbfFileName,Encoding encoding)
        {
            this.dbfFileName = dbfFileName;
            this._encoding = encoding;
 
            _writer = new BinaryWriter(File.Create(dbfFileName), _encoding);
        }

        public DbfFile(string dbfFileName)
            : this(dbfFileName,Encoding.ASCII)
        {
        }

        public void Write(DbfHeader header)
        {
            if (header == null)
                throw new ArgumentNullException("header");
 
            _headerWritten = true;

            // Set the encoding if not already done.
            if (!Equals(header.Encoding, _encoding))
            {
                header.Encoding = _encoding;
            }

            // Get the current position
            int currentPosition = (int)_writer.BaseStream.Position;

            //Header should always be written first in the file
            if (_writer.BaseStream.Position != 0)
                _writer.Seek(0, SeekOrigin.Begin);

            // actually write the header
            header.WriteHeader(_writer);

            // reposition the stream
            if (currentPosition != 0)
                _writer.Seek(currentPosition, SeekOrigin.Begin);

            _header = header;

        }

        public void Write(object[] columnValues)
        {
            if (columnValues == null)
                throw new ArgumentNullException("columnValues");
            if (!_headerWritten)
                throw new InvalidOperationException("Header records need to be written first.");
            int i = 0;

            // Check integrety of data
            if (columnValues.Length != _header.NumFields)
                throw new ArgumentException("The number of provided values does not match the number of fields defined", "columnValues");

            // Get the current position
            long initialPosition = _writer.BaseStream.Position;

            // the deleted flag
            _writer.Write((byte)0x20);
            foreach (object columnValue in columnValues)
            {
                var headerField = _header.Fields[i];

                if (columnValue == null || columnValue == DBNull.Value)
                {
                    // Don't corrupt the file by not writing if the value is null.
                    // Instead, treat it like an empty string.
                    Write(string.Empty, headerField.Length);
                }
                else if (headerField.Type == typeof(string))
                {
                    // If the column is a character column, the values in that
                    // column should be treated as text, even if the column value
                    // is not a string.
                    Write(columnValue.ToString(), headerField.Length);
                }
                else if (IsRealType(columnValue.GetType()))
                {
                    Write(Convert.ToDecimal(columnValue), headerField.Length, headerField.DecimalCount);
                }
                else if (IsIntegerType(columnValue.GetType()))
                {
                    Write(Convert.ToDecimal(columnValue), headerField.Length, headerField.DecimalCount);
                }
                else if (columnValue is decimal)
                {
                    Write((decimal)columnValue, headerField.Length, headerField.DecimalCount);
                }
                else if (columnValue is bool)
                {
                    Write((bool)columnValue);
                }
                else if (columnValue is string)
                {
                    Write((string)columnValue, headerField.Length);
                }
                else if (columnValue is DateTime)
                {
                    Write((DateTime)columnValue);
                }
                else if (columnValue is char)
                {
                    Write((char)columnValue, headerField.Length);
                }
                else
                {
                    throw new ArgumentException(
                        string.Format("Invalid argument for column '{0}': {1}",  headerField.Name, columnValue), "columnValues");
                }
                i++;
            }

            // Get the number of bytes written
            long bytesWritten = _writer.BaseStream.Position - initialPosition;

            // Get the record length (at least one byte for the deleted marker)
            int recordLength = Math.Max(1, _header.RecordLength);

            // Check if the correct amount of bytes was written
            if (bytesWritten != recordLength)
                throw new Exception("Error writing Dbase record");
        }
     
        public static bool IsRealType(Type type)
        {
            return ((type == typeof(double)) || (type == typeof(float)));
        }
     
        public static bool IsIntegerType(Type type)
        {
            return ((type == typeof(short)) || (type == typeof(int)) || (type == typeof(long)) ||
                    (type == typeof(ushort)) || (type == typeof(uint)) || (type == typeof(ulong)));
        }
     
        private void Write(decimal number, int length, int decimalCount)
        {
            string outString;

            int wholeLength = length;
            if (decimalCount > 0)
                wholeLength -= (decimalCount + 1);

            // Force to use point as decimal separator
            string strNum = Convert.ToString(number);
            int decimalIndex = strNum.IndexOf('.');
            if (decimalIndex < 0)
                decimalIndex = strNum.Length;

            if (decimalIndex > wholeLength)
            {
                // Too many digits to the left of the decimal. Use the left
                // most "wholeLength" number of digits. All values to the right
                // of the decimal will be '0'.
                var sb = new StringBuilder();
                sb.Append(strNum.Substring(0, wholeLength));
                if (decimalCount > 0)
                {
                    sb.Append('.');
                    for (int i = 0; i < decimalCount; ++i)
                        sb.Append('0');
                }
                outString = sb.ToString();
            }
            else
            {
                // Chop extra digits to the right of the decimal.
                var sb = new StringBuilder();
                sb.Append("{0:0");
                if (decimalCount > 0)
                {
                    sb.Append('.');
                    for (int i = 0; i < decimalCount; ++i)
                        sb.Append('0');
                }
                sb.Append('}');
                // Force to use point as decimal separator
                outString = string.Format(sb.ToString(), number);
            }

            for (int i = 0; i < length - outString.Length; i++)
                _writer.Write((byte)0x20);
            foreach (char c in outString)
                _writer.Write(c);
        }

        /// <summary>
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        private void Write(string text, int length)
        {
            // ensure string is not too big, multibyte encodings can cause more bytes to be written
            byte[] bytes = _encoding.GetBytes(text);
            int counter = 0;
            foreach (byte c in bytes)
            {
                _writer.Write(c);
                counter++;
                if (counter >= length)
                    break;
            }

            // pad the text after exact byte count is known
            int padding = length - counter;
            for (int i = 0; i < padding; i++)
                _writer.Write((byte)0x20);
        }

        /// <summary>
        /// </summary>
        /// <param name="date"></param>
        private void Write(DateTime date)
        {
            foreach (char c in date.Year.ToString(NumberFormatInfo.InvariantInfo))
                _writer.Write(c);

            if (date.Month < 10)
                _writer.Write('0');
            foreach (char c in date.Month.ToString(NumberFormatInfo.InvariantInfo))
                _writer.Write(c);

            if (date.Day < 10)
                _writer.Write('0');
            foreach (char c in date.Day.ToString(NumberFormatInfo.InvariantInfo))
                _writer.Write(c);
        }

        /// <summary>
        /// </summary>
        /// <param name="flag"></param>
        private void Write(bool flag)
        {
            _writer.Write(flag ? 'T' : 'F');
        }

        /// <summary>
        ///     Write a character to the file.
        /// </summary>
        /// <param name="c">The character to write.</param>
        /// <param name="length">
        ///     The length of the column to write in. Writes
        ///     left justified, filling with spaces.
        /// </param>
        private void Write(char c, int length)
        {
            string str = string.Empty;
            str += c;
            Write(str, length);
        }

        public void WriteEndOfDbf()
        {
            Write((byte)0x1A);
        }

        /// <summary>
        ///     Write a byte to the file.
        /// </summary>
        /// <param name="number">The byte.</param>
        private void Write(byte number)
        {
            _writer.Write(number);
        }

        /// <summary>
        /// Method to close this dbase file writer
        /// </summary>
        public void Close()
        {
            _writer.Close();
        }
        public bool IsDisposed { get; private set; }
        /// <summary>
        /// Method to dispose this writers instance
        /// </summary>
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                Dispose(true);
                GC.SuppressFinalize(this);

            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                Close();
            }
        }

    }
}
