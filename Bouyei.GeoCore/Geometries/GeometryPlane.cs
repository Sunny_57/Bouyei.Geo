﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.GeoCore.Geometries
{
    using GeoParsers;

    public class GeometryPlane:GeometryBase
    {
        public GeometryPlane(int zoneWide = 3, int precision = 0)
            : base(zoneWide: zoneWide, precision: precision)
        { }

        public GeometryPlane(double lRadius/*长半轴*/,
            double sRadius/*短半轴*/,
            int zoneWide = 3,
            int precision = 0/*精度保留小数点位*/)
            : base(lRadius, sRadius, precision, zoneWide)
        {

        }

        public double Area(Coordinate[] coords)
        {
            bool isLB = coords[0].X < 360;

            if (isLB)
            {
                for (int i = 0; i < coords.Length; ++i)
                {
                    coords[i] = LBtoXY(coords[i]);//需要转换为平面直角坐标
                }
            }

            int len = coords.Length - 1;
            double s = 0, x, _y;

            double x0 = coords[0].X;

            for (int i = 1; i < len; ++i)
            {
                x = coords[i].X - x0;
                _y = (coords[i - 1].Y - coords[i + 1].Y);
                s += x * _y;
            }

            s *= 0.5;
            if (precision > 0)
            {
                s = Math.Round(s, precision);
            }
            return s;
        }

        public double Area(Geometry geometry)
        {
            double sum = 0;

            for (int i = 0; i < geometry.GeometryCount; ++i)
            {
                var geo = geometry.GetGemoetry(i);
                for (int j = 0; j < geo.Count; ++j)
                {
                    var seq = geo[i];
                    sum += Area(seq.GetCoordinates());
                }
            }

            return Math.Abs(sum);
        }

        public double Area(string wkt)
        {
            var wktParser = new WktParser(wkt);
            var colleciton = wktParser.FromReader();

            var sum = Area(colleciton[0]);

            double holes = 0.0;
            //图形有洞
            for (int i = 1; i < colleciton.Count; ++i)
            {
                holes += Area(colleciton[i]);
            }

            return Math.Abs(sum + holes);
        }
    }
}
