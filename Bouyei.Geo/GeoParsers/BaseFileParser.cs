﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.Geo.GeoParsers
{
    public class BaseFileParser : IParser
    {
        protected string filename;
        protected Encoding encoding = Encoding.UTF8;
        public BaseFileParser(string filename)
        {
            this.filename = filename;
        }

        public BaseFileParser(string filename,Encoding encoding)
            :this(filename)
        {
            this.encoding = encoding;
        }

        public virtual void Dispose()
        { }
    }
}
