﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.Geo.Geometries
{
    public class GeoMultiLinestring : Geometry
    {
        public GeoLinestring this[int index]
        {
            get { return new GeoLinestring(GetSequence(0,index)); }
        }

        public GeoMultiLinestring(List<Coordinate[]> coordinates)
            : base(GeoType.MULTILINESTRING, coordinates)
        { }

        public GeoMultiLinestring(string wktString)
            : base(GeoType.MULTILINESTRING, wktString)
        { }
    }
}
