﻿using Bouyei.DbFactory;
using Bystd.Geo.Geometries;
using Bystd.Geo.GeoParsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bystd.GeoDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestFromEsriMdb();
            TestXYLB();
        }

        static void TestXYLB()
        {
            GeometryBase gbase = new GeometryBase();
            var start = new Coordinate() { X = 107.8189714, Y = 26.16855608 };
            var end = new Coordinate() { X = 107.4911, Y = 26.09580 };

            var pend = gbase.LBtoXY(end);

            var L = gbase.Distance(start, end);
        }

        static void TestFromEsriMdb()
        {
            string connstr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\\a.mdb;";
            using (IAdoProvider provider = AdoProvider.CreateProvider(connstr, FactoryType.OleDb))
            {
                var rt = provider.Query<Item>(new Parameter("select SHAPE as wkb from a"));
                var items = rt.Result;
                List<Geometry> geos = new List<Geometry>();
                foreach (var item in items)
                {
                    EsriMdbParser wkbParser = new EsriMdbParser(item.wkb);
                    var geo = wkbParser.FromReader();

                    //生成wkt
                    string wkt = geo.ToWkt();

                    geos.Add(geo);
                }
            }
        }
    }

    public class Item
    {
        public string bsm { get; set; }

        public byte[] wkb { get; set; }

    }
}
