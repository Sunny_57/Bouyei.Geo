﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bystd.Geo.Geometries
{
    public class GeoMultiPoint:Geometry
    {
        public GeoMultiPoint(string wktString)
        :base(GeoType.MULTIPOINT,wktString)
        { }

        public GeoMultiPoint(Coordinate[] coordinates)
            : base(GeoType.MULTIPOINT, coordinates)
        { }

        public Coordinate this[int index]
        {
            get { return GetSequence(0, 0)[index]; }
        }
    }
}
